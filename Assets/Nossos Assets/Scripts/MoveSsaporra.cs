﻿using UnityEngine;
using System.Collections;

public class MoveSsaporra : MonoBehaviour {

    public float Speed = 0f;
    private float movex = 0f;

    //public float jumpSpeed = 10.0f;
    public static bool noChao = false;
    public Transform groundCheck;
    public float groundRadius = 0.2f;
    public LayerMask whatIsGround;

    Rigidbody2D player;
    GameObject playerObj;

    void Awake() {
        groundCheck = GameObject.Find("GroundCheck").transform;
    }

    // Update is called once per frame
    void FixedUpdate(){
        noChao = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

        GetComponent<Rigidbody2D>().velocity = new Vector2(movex * Speed, GetComponent<Rigidbody2D>().velocity.y);

    }

    void Update() {
        movex = Input.GetAxis("Horizontal");

        /*if (PulaPula.MoveMeUp() && noChao) {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpSpeed));

        }
        if (noChao) Debug.Log("sanunsai");*/
    }

    public static bool getnoChao()
    {
        return noChao;
    }


}
