﻿using UnityEngine;
using System.Collections;

public class PulaPula : MonoBehaviour {
    static Rigidbody2D player;
    GameObject playerObj;

    public float jumpSpeed = 1500.0f;

    // Use this for initialization
    void Start () {
        playerObj = GameObject.Find("FROGY");
        player = playerObj.GetComponent<Rigidbody2D>();
    }
    public void MoveMeUp()
    {
        if (MoveSsaporra.getnoChao())
        {
            player.AddForce(new Vector2(0, jumpSpeed));

        }
    }
}
